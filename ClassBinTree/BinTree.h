/*
 * BinTree.h
 *
 *  Created on: 14.04.2013
 *      Author: Aprogrom
 */

#ifndef BINTREE_H_
#define BINTREE_H_
#include "../ClassPoint/Point.h"
class BinTree{
private:
	int count;
	Point* binTreeFirstElem;
	Point* now;
public:
	BinTree(int FirstElem);//�����������, ������� ���, ����
	Point* getFirst();//���������� ��������� �� ������.
	Point* getLeftEnd();//���������� ��������� �������
	void add(int elem);//���������� ��������(���������� ������ � ����������)
	Point* find(int findElem);//����� � ������� ��������� �� ������� �� ��������� ������ �����������
	void walkTree_CLR(Point* node);//����� ����� ������ ����
	void walkTree_CRL(Point* node);//����� ����� ������ ����
//	void walkTree_PLR(bool flag = 0);//����� ����� ������ ����
//	bool remove(int removeElem);
//	bool unionTree();
//	bool rebalance();
};


#endif /* BINTREE_H_ */
