/*
 * Point.cpp
 *
 *  Created on: 14.04.2013
 *      Author: Aprogrom
 */
#include "Point.h"
#include <iostream>

using namespace std;
Point::~Point(){
	delete this;
}//����������
Point* Point::getParent(){
	return this->parent;
}//���������� ��������� �� ��������
Point* Point::getRight(){
	return this->right;
}//���������� ��������� �� ������� �������
Point* 	Point::getLeft(){
	return this->left;
}//������� ��������� �� ������ �������
int Point::getElem(){
	return this->elem;
}//���������� �������� ��������
void Point::addRight(int elem){
	Point* right = new Point(elem, this, 'r');
	this->right = right;
}//�������� ������� �������
void Point::addLeft(int elem){
	Point* left = new Point(elem, this, 'l');
	this->left = left;
}//�������� ������ �������
bool Point::nextExist(){
	if((this->right != 0) || (this->left != 0))return 1;
	else return 0;
}//�������� �� ������������� ��������, ���� �� ������
bool Point::nextRightExist(){
	return (this->right != 0)? 1: 0;
}//�������� �� ������������� �������
bool Point::nextLeftExist(){
	return (this->left != 0)? 1: 0;
}//�������� �� ������������� ������
bool Point::compare(int elem){
	return elem >= this->elem;
}//��������� ����������� �������� � �������� ��������
bool Point::yes(int point){
	if(point == this->elem){
		return 1;
	}else{
		return 0;
	}
}//������� ��������� �� ��������� ����������� ������� � �������� ��������


