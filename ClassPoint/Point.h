/*
 * Point.h
 *
 *  Created on: 14.04.2013
 *      Author: Aprogrom
 */

#ifndef POINT_H_
#define POINT_H_

/*template <typename T>*/ class  Point{
private:
	int elem;
	Point* parent;
	Point* right;
	Point* left;
	char rol;
public:
//	Point(int newElem, Point* parent = 0, char rol = 'p'){//����������� �����
//		this->elem = newElem;//
//		this->parent = parent;//
//		this->right = 0;//
//		this->left = 0;//
//		this->rol = rol;//
//	};
	Point(int newElem, Point* parent = 0, char rol = 'p'){//����������� �����
		this->elem = newElem;//
		this->parent = parent;//
		this->right = 0;//
		this->left = 0;//
		this->rol = rol;//
	};//����������� �����
	~Point();//����������
	Point* getParent();//���������� ��������� �� ��������
	Point* getRight();//���������� ��������� �� ������� �������
	Point* getLeft();//������� ��������� �� ������ �������
	int getElem();//���������� �������� ��������
	void addRight(int elem);//�������� ������� �������
	void addLeft(int elem);//�������� ������ �������
	bool nextExist();//�������� �� ������������� ��������, ���� �� ������
	bool nextRightExist();//�������� �� ������������� �������
	bool nextLeftExist();//�������� �� ������������� ������
	bool compare(int elem);//��������� ����������� �������� � �������� ��������
	bool yes(int point);//������� ��������� �� ��������� ����������� ������� � �������� ��������
};


#endif /* POINT_H_ */
