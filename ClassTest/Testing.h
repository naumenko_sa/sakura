/*
 * Testing.h
 *
 *  Created on: 18.04.2013
 *      Author: Aprogrom
 */

#ifndef TESTING_H_
#define TESTING_H_


class Testing{
private:
	enum PointOfMenu{
		START, WAIT ,ADD , FIND, REMOVE, SHOW, REBALANCE, UNION, STOP
	};
	PointOfMenu now;
public:
	Testing();
	void start();
	void continueP();
};

#endif /* TESTING_H_ */
