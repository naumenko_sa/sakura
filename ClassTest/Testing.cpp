/*
 * Testing.cpp
 *
 *  Created on: 18.04.2013
 *      Author: Aprogrom
 */
#include <stdio.h>
#include <stdlib.h>
#include "Testing.h"

Testing::Testing(){
	now = START;
}
void Testing::start(){
	if(this->now == START){
		printf("Testing BinTree started. You can choose point of menu:\n");
		FILE* file;
		if(!(file = fopen("PointOfMenu", "r"))){
			printf("Error. Can't open file");
			exit(1);
		}
		for(int i = 1; i < 7; i++){
			int n;
			char str[10];
			scanf("%s %d\n", str, &n);
//			if(n != 0){
				printf("%d - %s;\n",i , str);
//			}
		}
		this->now = WAIT;
	}
}
//	void Testing::continueP(){
//		while(this->now == WAIT){
//
//		}
//	}

